import Component from '@glimmer/component'
import { tracked } from '@glimmer/tracking'
import { action } from '@ember/object'

interface ImportsSignature {
  // The arguments accepted by the component
  Args: {}
  // Any blocks yielded by the component
  Blocks: {
    default: []
  }
  // The element to which `...attributes` is applied in the component template
  Element: null
}

import * as ecPair from 'ecpair';
console.log(ecPair);

// import { ECPairFactory, networks } from 'ecpair'
// console.log('networks:', networks)
// console.log('ECPairFactory:', ECPairFactory)
// const logger = new LoggerClass.Logger()
// console.log('logger: ', logger)
//console.log('Various imports:', VariousImports)

export default class ImportsComponent extends Component<ImportsSignature> {
  @tracked res = 'none';

  @action async button1() {
    this.res = 'todo'
  }

}

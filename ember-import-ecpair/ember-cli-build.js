'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function (defaults) {
  const app = new EmberApp(defaults, {
    'ember-cli-babel': { enableTypeScriptTransform: true },

    // Add options here
  });

  const { Webpack } = require('@embroider/webpack');
  const webpack = require('webpack')

  return require('@embroider/compat').compatBuild(app, Webpack, {
    skipBabel: [
      {
        package: 'qunit',
      },
    ],
    packagerOptions: {
      webpackConfig: {
        resolve: {
          fallback: {
            // assert: require.resolve('assert/'),
            // buffer: require.resolve('buffer/'),
            // crypto: require.resolve('crypto-browserify'),
            // events: require.resolve('events/'),
            stream: require.resolve('stream-browserify'),
          },
        },
        plugins: [
          new webpack.ProvidePlugin({
            Buffer: ['buffer', 'Buffer'],
            //process: 'process/browser',
          }),
        ],
      },
    },
  });
};

plugins {
    kotlin("jvm") version "2.1.0"
    kotlin("plugin.serialization") version "2.1.0"
    application
}

group = "wf.matrix"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.7.3")
    // implementation("org.hildan.krossbow:krossbow-stomp-core:6.0.0")
    implementation("org.hildan.krossbow:krossbow-websocket-builtin:8.1.0")
    implementation("org.hildan.krossbow:krossbow-stomp-kxserialization-json:8.1.0")
    testImplementation("org.jetbrains.kotlin:kotlin-test")
    implementation(kotlin("stdlib-jdk8"))
}

tasks.test {
    useJUnitPlatform()
}
kotlin {
    jvmToolchain(21)
    jvmToolchain(21)
}

//task execute(type:JavaExec) {
//    main = 'wf.matrix.ProvaKt'
//    classpath = sourceSets.main.runtimeClasspath
//}

application {
    mainClass.set("wf.matrix.MainKt") // The main class of the application
}

package wf.matrix

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import org.hildan.krossbow.stomp.StompClient
import org.hildan.krossbow.stomp.conversions.kxserialization.StompSessionWithKxSerialization
import org.hildan.krossbow.stomp.conversions.kxserialization.json.withJsonConversions
import org.hildan.krossbow.stomp.conversions.kxserialization.subscribe
import org.hildan.krossbow.stomp.frame.FrameBody
import org.hildan.krossbow.stomp.headers.StompSendHeaders
import org.hildan.krossbow.websocket.WebSocketClient
import org.hildan.krossbow.websocket.builtin.builtIn

//import java.util.concurrent.CompletableFuture

data class RpcCallData(
//    val future: CompletableFuture<JsonObject>,
    val deferred: CompletableDeferred<JsonObject>,
    val headers: StompSendHeaders,
    val body: FrameBody.Text,
    //val data: JsonObject?,
    var numRetries: Int,
)

@Serializable
data class RpcReplyDTO(
    val id: String,
    val m: JsonObject,
)

@Serializable
data class RpcErrorDTO(
    val id: String,
    val ex: String,
    val msg: String,
    //val param: Map<String, String> = emptyMap<String, String>(),
    val param: JsonObject? = null,
    val attemptsLeft: Int = -1
)

@Serializable
data class BlockDTO(
    val coin: String,
    val hash: String,
    val prevHash: String,
    val height: Int,
    val time: Long,
    val chainWork: String,
    val orphanedChain: Boolean = false
)

class StompTransport {
    val discoveryUrl = "http://localhost:9090/api/v1/endpoint/stomp"

    // TODO: Heartbeat non funzionante!
    private val stompClient = StompClient(WebSocketClient.builtIn())
//    private val stompClient = StompClient(WebSocketClient.builtIn()) {
//        heartBeat = HeartBeat(50.seconds, 50.seconds)
//        heartBeatTolerance = HeartBeatTolerance(Duration.ZERO, 10.seconds)
//    }

    suspend fun connect(url: String): StompRpc {
        val session = stompClient.connect(url).withJsonConversions()
        val rpcReplyFlow: Flow<RpcReplyDTO> = session.subscribe("/user/queue/rpc", RpcReplyDTO.serializer())
        val rpcErrorFlow: Flow<RpcErrorDTO> = session.subscribe("/user/queue/errors", RpcErrorDTO.serializer())
//        val rpcErrorFlow2: Flow<String> = session.subscribeText("/user/queue/errors")
//        stompSession.subscribe("/topic/games", GameListEventWrapper.serializer()).map { it.event }
        return StompRpc(rpcReplyFlow, rpcErrorFlow, session)
    }
}

class StompRpc(
    private val rpcReplyFlow: Flow<RpcReplyDTO>,
    private val rpcErrorFlow: Flow<RpcErrorDTO>,
    //private val rpcErrorFlow2: Flow<String>,
    private val stompSession: StompSessionWithKxSerialization
) {

    init {
        val scope = CoroutineScope(Dispatchers.Default)
        scope.launch {
            rpcReplyFlow.collect(::rpcResultHandler)// { msg -> println("RPC REPLY: $msg") }
        }
        scope.launch {
            rpcErrorFlow.collect(::rpcErrorHandler)// { msg -> println("RPC ERROR: $msg") }
        }
    }

    suspend fun disconnect() = stompSession.disconnect()

    suspend fun watchBlocks(): Flow<BlockDTO> = stompSession.subscribe("/topic/blocks", BlockDTO.serializer())

    private var rpcCounter = 0
    private val waitingReplies: MutableMap<String, RpcCallData> = mutableMapOf()
    private val rpcMaxRetries = 10
    private val rpcRetryDelay: Long = 1000

//    suspend fun rpcWithTimeout(queue: String, args: JsonObject? = null, retryNum: Int = 1): Deferred<JsonObject> {
//        val job = rpc(queue, args, retryNum)
//    }

    suspend fun rpc(queue: String, args: JsonObject? = null, retryNum: Int = 1): Deferred<JsonObject> {
        val rpcId = (++rpcCounter).toString()
//       TODO: per-request maxRetries and timeout
//        const maxRetries = opts.numRetries || this.rpcMaxRetries
//        const rpcTimeout = opts.rpcTimeout || this.rpcTimeout
        val headers = StompSendHeaders(
            destination = queue,
            customHeaders = mapOf("id" to rpcId),
        )
        val body = FrameBody.Text(Json.encodeToString(args))
        println("RPC Call to ${headers.destination} with id $rpcId and body: $body")
        val deferred = CompletableDeferred<JsonObject>()
        waitingReplies[rpcId] = RpcCallData(deferred, headers, body, retryNum)
//       TODO: signal RPC call waiting
//        if (waitingReplies.isEmpty())
//            this.emit(MelisEvent.EVENT_RPC_ACTIVITY_START)
        stompSession.send(headers, body)
        return deferred
    }

    private fun rpcResultHandler(rpcReply: RpcReplyDTO) {
        val id = rpcReply.id
        val rpcData = waitingReplies.remove(id)
        if (rpcData == null) {
            println("[RPC] Unable to find RPC call #$id in waitingReplies")
            return
        }
        println("[RPC] Completing RPC call $id with: $rpcData")
        rpcData.deferred.complete(rpcReply.m)
    }

    // TODO: Spostare nell'apposita classe
    val TOO_MANY_REQUESTS = "CmTooManyRequestException"

    private suspend fun rpcErrorHandler(rpcError: RpcErrorDTO) {
        val id = rpcError.id
        println("[RPCEX] RPC call $id with exception: $rpcError")
        val rpcData = waitingReplies[id]
        if (rpcData == null) {
            println("[RPC ERR] Unable to find RPC call #$id in waitingReplies")
            return
        }
        if (rpcError.ex == TOO_MANY_REQUESTS) {
            if (rpcData.numRetries >= rpcMaxRetries) {
                rpcData.deferred.completeExceptionally(MelisException("SERVER_ERR", "Too many request retries"))
            } else {
                val rpcRetryDelay = rpcRetryDelay * rpcData.numRetries
                println("[RPCEX] Server requested to slow down request #${id} -- retry #${rpcData.numRetries} waiting $rpcRetryDelay ms")
                delay(rpcRetryDelay)
                rpcData.numRetries++
                stompSession.send(rpcData.headers, rpcData.body)
            }
        } else {
            rpcData.deferred.completeExceptionally(MelisException.fromServerEx(rpcError))
        }
    }

}

/*

PER RIFERIMENTO

suspend fun StompSession.sendText(
    destination: String,
    myHeader1: String, // could be another type
    myHeader2: String,
    body: String,
) {
    val headers = StompSendHeaders(
        destination = destination,
        customHeaders = mapOf(
            "My-Header-1" to myHeader1,
            "My-Header-2" to myHeader2,
        ),
    )
    send(headers, FrameBody.Text(body))
}
 */
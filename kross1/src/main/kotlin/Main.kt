package wf.matrix

import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlin.time.Duration.Companion.seconds

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
fun main() {
    runBlocking { main2() }
}

//@Serializable
//data class MyMessage(val timestamp: Long, val author: String, val content: String)

const val wsUrl = "ws://localhost:9090/stomp"

suspend fun main2() = runBlocking { // this: CoroutineScope
    launch { // launch a new coroutine and continue
        val client = StompTransport()
        val session = client.connect(wsUrl)
        println("Connected to ${wsUrl}")
        val blocksQueue = session.watchBlocks()
        val blocksJob = this.launch { blocksQueue.collect({ block -> println("Block received: $block") }) }
        val challenge = session.rpc("/app/v1/wallet/getChallenge")
        val version = session.rpc("/app/system/version")
        val badParamResult = session.rpc("/app/system/debug/throwBadParamEx")
        //session.rpc("/app//v1/public/getPaymentAddress")
        println("RPC sent, Waiting for replies...")
        println("challenge: ${challenge.await()}")
        println("version: ${version.await()}")
        try {
            println("badParamResult: ${badParamResult.await()}")
        } catch (e: MelisException) {
            println("RPC badParamResult completed with exception ${e.ex} : ${e.message}")
        }
        val slowResult = session.rpc("/app/system/debug/sleep/5000")
        try {
            withTimeout(1000) {
                println("  ---> ERROR: should have not gotten slowResult: ${slowResult.await()}")
            }
        } catch (e: Exception) {
            println("RPC slowResult threw exception ${e}")
        }
        var counter = session.rpc("/app/system/debug/counter")
        println("current counter: ${counter.await()}")
        //counter.join()
        counter = session.rpc("/app/system/debug/counter/0")
        println("setCounter: ${counter.await()}")

        val retryResult =
            session.rpc("/app/system/debug/throwTooManyRequestEx", JsonObject(mapOf("data" to JsonPrimitive("5"))))
        try {
            println("retryResult: ${retryResult.await()}")
        } catch (e: MelisException) {
            println("RPC retryResult completed with exception ${e}")
        }
        delay(10.seconds)
        println("Waiting finished, cancelling receive jobs")
        blocksJob.cancel()
    }
    println("MAIN2 executing")
}

//suspend fun main3() = runBlocking { // this: CoroutineScope
//    launch { // launch a new coroutine and continue
//        val client = StompClient(WebSocketClient.builtIn()) // other config can be passed in here
//        val session: StompSession = client.connect(wsUrl) // optional login/passcode can be provided here
//
//        delay(1000L) // non-blocking delay for 1 second (default time unit is ms)
//
//        session.use { s ->
//            val rpcReply: Flow<String> = s.subscribeText("/user/queue/rpc")
//            // val rpcReply: Flow<MyMessage> = s.subscribe("/user/queue/rpc", MyMessage.serializer())
//            val rpcError: Flow<String> = s.subscribeText("/user/queue/errors")
//            val blocksQueue: Flow<String> = s.subscribeText("/topic/blocks")
//
//            val configReply = s.subscribe("/app/v1/config")
//            val configJob = this.launch {
//                configReply.collect { msg -> println("CONFIG Reply: $msg") }
//            }
//
//            val job2 = this.launch {
//                rpcReply.collect { msg -> println("RPC REPLY: $msg") }
//            }
//            val job3 = this.launch {
//                rpcError.collect { msg -> println("RPC ERROR: $msg") }
//            }
//            val job4 = this.launch {
//                blocksQueue.collect { msg -> println("New Block: $msg") }
//            }
//
//            //s.sendEmptyMsg(destination = "/app/v1/wallet/getChallenge")
//            //r = s.convertAndSend(destination = "/v1/utils/getChallenge", body = "{}")
//            //println("r2: ${r}")
//
//            println("Waiting replies...")
//            delay(1.hours)
//            configJob.cancel()
//            job2.cancel()
//            job3.cancel()
//            job4.cancel()
//        }
//    }
//    println("MAIN1") // main coroutine continues while a previous one is delayed
//}
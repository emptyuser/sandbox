package wf.matrix

class Prova {

//    suspend fun test1() {
//        val url = "http://localhost:9090/api/v1/endpoint/stomp"
//        val client = StompClient(WebSocketClient.builtIn()) // other config can be passed in here
//        val session: StompSession = client.connect(url) // optional login/passcode can be provided here
//
//// Send text messages using this convenience function
//        session.sendText(destination = "/some/destination", body = "Basic text message")
//
//// Sometimes no message body is necessary
//        session.sendEmptyMsg(destination = "/some/destination")
//
//// This subscribe() call triggers a SUBSCRIBE frame
//// and returns the flow of messages for the subscription
//        val subscription: Flow<String> = session.subscribeText("/some/topic/destination")
//
//// Use an appropriate coroutine 'scope' to collect the received frames
//        val collectorJob = scope.launch {
//            subscription.collect { msg ->
//                println("Received: $msg")
//            }
//        }
//        delay(3000)
//// cancelling the flow collector triggers an UNSUBSCRIBE frame
//        collectorJob.cancel()
//
//        session.disconnect()
//    }

}

open class StompRPCError(message: String) : Exception(message) {}

open class MelisException(val ex: String, message: String) : StompRPCError(message) {
    companion object {
        fun fromServerEx(rpcError: RpcErrorDTO): MelisException = MelisException(rpcError.ex, rpcError.msg)
    }

    fun getMelisEx() = ex
}

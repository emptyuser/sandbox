import Component from '@glimmer/component'
import { tracked } from '@glimmer/tracking'
import { action } from '@ember/object'
import { cached } from '@glimmer/tracking'
import { TrackedAsyncData } from 'ember-async-data'

interface ImportsSignature {
  // The arguments accepted by the component
  Args: {}
  // Any blocks yielded by the component
  Blocks: {
    default: []
  }
  // The element to which `...attributes` is applied in the component template
  Element: null
}


import {
  LoggerClass, MieiTipi, VariousImports
} from 'sample-ts-esm1'

//console.log('Logger class:', LoggerClass)
// const logger = new LoggerClass.Logger()
// console.log('logger: ', logger)
//console.log('Various imports:', VariousImports)

let BIP32

export default class ImportsComponent extends Component<ImportsSignature> {
  @tracked res = 'none';
  @tracked signed = 'x';

  @cached
  get lib(): TrackedAsyncData {
    const somePromise = import('sample-ts-esm1')
    console.log("'executing lib getter, promise: ", somePromise)
    somePromise.then(x => {
      console.log('promise loaded: ', x)
      BIP32 = x.VariousImports.BIP32
    })
    return new TrackedAsyncData(somePromise)
  }

  // @cached
  // getLib() {
  //   const somePromise = ImportazioniVarie.ProvaImportazioni.f('cxii')
  //   return new TrackedAsyncData(somePromise);
  // }

  @action async button1() {
    const l = await this.lib
    console.log('tracked state:', l.state)
    console.log('tracked isResolved:', l.isResolved)
    //console.log('tracked value:', l.value)
    console.log('bip32: ', BIP32)
    this.res = 'todo'
    //const x = require('/dinamica.js')
    // const IMP = require('sample-ts-esm1')
    // console.log("Dynamic import: ", IMP)
  }

  @action signMessage() {
    // const key =
    //   'xprv9s21ZrQH143K2GpDi1A3WNnERLN2HLQohKGQ5g4xCLywEjbmhdgM64ppdSLiG8kzRw77sxH7o2bBtxfJSgawCtnGpe1ETWjGYR3vwoJS4BV'
    // const message = 'A test message'
    // //    const hd = BIP32.fromBase58(key)
    this.signed = 'todo'
  }
}

//test import from local modules
import { GenericStringHash, COINS } from './tipi'

export class Logger {

  logObj: any = console
  prefix: string = COINS.BCH + " -- "

  constructor(opts: GenericStringHash = {}) {
    if (opts.loggerObject)
      this.logObj = opts.loggerObject
    if (opts.prefix)
      this.prefix = opts.prefix
  }

  setPrefix(p: string) {
    this.prefix = p
  }

  setLogObject(newObj: any) {
    this.logObj = newObj
  }

  log(a: any, b?: any) {
    if (a && b)
      this.logObj.log(a, b)
    else
      this.logObj.log(a)
  }

  logWarning(a: any, b?: any) {
    if (a && b)
      this.logObj.warn(a, b)
    else
      this.logObj.warn(a)
  }

  logError(a: any, b?: any) {
    if (a && b)
      this.logObj.error(a, b)
    else
      this.logObj.error(a)
  }

}

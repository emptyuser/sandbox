export enum COINS {
  BTC = "BTC",
  BCH = "BCH",
  LTC = "LTC",
  GRS = "GRS",
  BSV = "BSV",
  ABC = "ABC",
  DOGE = "DOGE",
}

export interface StringHash { [key: string]: string }
export interface GenericStringHash { [key: string]: any }

export interface IPagingInfo extends GenericStringHash {
  page?: number
  size?: number
  sortField?: string
  sortDir?: string
}

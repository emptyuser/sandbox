import { GenericStringHash, COINS } from './tipi'

import * as ECC from 'tiny-secp256k1'
import { BIP32API, BIP32Factory, BIP32Interface } from 'bip32'
import { ECPairFactory, ECPairAPI } from 'ecpair'
console.log("ECPAIRFACTORY: ", ECPairFactory)
const ECPair: ECPairAPI = ECPairFactory(ECC)
export const BIP32:BIP32API = BIP32Factory(ECC)

export class TestClass {
  public x: string = 'dummy ' + COINS.BTC

  adder(n: number): number { return n + 1 }

  // async buildEcPair(): ECPairAPI {
  //   return ECPairFactory(ECC)
  // }

  // static f(s: string = 'no par'): string {
  //   //return "" + ECC.isPoint
  //   return "BIP32 factory name: " + s + BIP32Factory.name
  // }

}
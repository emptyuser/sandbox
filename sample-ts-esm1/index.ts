// direct export
export class MyConstants {
  static readonly URL_PRODUCTION = "https://xxx"
  static readonly URL_TESTNET = "https://yyy"

  static readonly MAX_SUBPATH = 16777216
  static readonly SATOSHIS_ONE_BIT = 100
  static readonly SATOSHIS_ONE_MBTC = 100000
  static readonly SATOSHIS_ONE_BTC = 100000000
}

// rename exports
export * as MieiTipi from './src/tipi'
export * as LoggerClass from './src/logger-module'
export * as VariousImports from './src/various-imports'